#include "s21_string.h"

#define WIDTH_DEFAULT -1
#define LENGTH_DEFAULT ' '
#define BUFFER_SIZE 300
#define PRECISION_DEFAULT 1

int s21_start_processing_s(const char *str, const char *format, va_list argsPtr);
int s21_format_parsing_s(const char **formatPtr, Token*);
int s21_is_format_correct_s(Token *tokenPtr);
int s21_are_non_sscanf_fields_default_s(Token *tokenPtr);
void s21_init_struct_s(Token *);
int s21_is_specifier_s(char);
int s21_is_length_s(const char symb);
void s21_set_flag_s(const char, Token *);
int s21_set_field_s(const char **formatPtr, int *fieldPtr);
char *s21_get_digits_in_str_s(char *dest, const char *src, char *digitSymbols, int n);
int set_precision(const char **formatPtr, Token *token, va_list argsPtr);
int s21_call_writing_func_s(Token *token, const char **strPtr, va_list argsPtr, int *x);
int s21_write_to_int_s(Token *tokenPtr, const char **strPtr, va_list argsPtr, int *x);
int s21_write_to_float_s(Token *tokenPtr, const char **strPtr, va_list argsPtr, int *x);
int s21_write_to_char_s(Token *tokenPtr, const char **strPtr, va_list argsPtr, int *x);
int s21_write_to_str_s(Token *tokenPtr, const char **strPtr, va_list argsPtr, int *x);
int s21_put_char_s(char *buffer, Token *tokenPtr, va_list argsPtr, int *);
int s21_put_int_s(char *buffer, Token *tokenPtr, va_list argsPtr, int *);
int s21_put_float_s(char *buffer, Token *tokenPtr, va_list argsPtr, int *);
double s21_atof_s(char *buffer);
unsigned long int s21_cast16_to10_s(char *digitsStr);
int s21_cast8_to10_s(char *digitsStr);
int s21_is_flag_s(const char symb);
int s21_write_to_numb_s(Token *tokenPtr, va_list argsPtr, int *x);

int s21_sscanf(const char *str, const char *format, ...) {
    int successCount;
    va_list argsPtr;
    va_start(argsPtr, format);
    // check for % symbol
    if ((str != S21_NULL) && (s21_strchr(format, (int)'%') != S21_NULL)) {
        successCount = s21_start_processing_s(str, format, argsPtr);
    } else {
        successCount = 0;
    }
    va_end(argsPtr);
    return successCount;
}

int s21_start_processing_s(const char *str, const char *format, va_list argsPtr) {
    int successCount = 0;
    Token token;
    s21_init_struct_s(&token);
    int count_symb = 0;
    while (*format == ' ') {
        format++;
    }
    while (*format) {                                  // (*str && *format)
        if (*format == ' ') {
            format++;
            while (*str == ' ' || *str == '\0' || *str == '\n' || *str == '\t') {
                str++;
            }
        }
        if (*format != '%') {              // строки вне спецификаторов должны быть равны
            if (*format == *str) {
                str++;
                format++;
                continue;
            } else {
                return successCount;
            }
        } else {
            format++;                  // нашли % и начинаем парсить со следующего знака
            int allisGood = s21_format_parsing_s(&format, &token);
            if (allisGood) allisGood = s21_is_format_correct_s(&token);
            if (allisGood) allisGood = s21_call_writing_func_s(&token, &str, argsPtr, &count_symb);
            if (allisGood) {
                successCount++;
            } else {
                return successCount;
            }
        }
    }
    return successCount;
}

int s21_format_parsing_s(const char **formatPtr, Token *tokenPtr) {
    s21_init_struct_s(tokenPtr);
    int flag = 1;
    int spaceCount = 0;  // чтобы не залетал с пробелами посреди слова в первое условие
    while (*formatPtr && flag) {
        if ((**formatPtr) == '*') {
            tokenPtr->ignoreAssign = 1;
            (*formatPtr)++;
        } else if (s21_is_flag_s(**formatPtr) && spaceCount == 0) {  // %c", *formatPtr, (*token).specifier);
            s21_set_flag_s(**formatPtr, tokenPtr);
            (*formatPtr)++;
            spaceCount++;
        } else if (isdigit(**formatPtr)) {
            s21_set_field_s(formatPtr, &tokenPtr->WIDTH);   // set_width
        } else if (**formatPtr == '.') {       // подавлять присвоение
            (*formatPtr)++;                      // после точки
            flag = s21_set_field_s(formatPtr, &tokenPtr->PRECISION);   // set_precision
        } else if (s21_is_length_s(**formatPtr)) {
            tokenPtr->LENGTH = **formatPtr;
            (*formatPtr)++;
            spaceCount++;
        } else if (**formatPtr == '%' && *((*formatPtr) + 1) == '%') {  // need check %% in string and skip it
            *formatPtr += 2;
        } else if (s21_is_specifier_s(**formatPtr)) {
            tokenPtr->specifier = **formatPtr;
            (*formatPtr)++;
            break;
        } else {
            // (*formatPtr)++;
            flag = 0;
        }
    }
    return flag;
}

int s21_is_format_correct_s(Token *tokenPtr) {
    char specifier = tokenPtr->specifier;
    int flag = 1;
    int isInteger = S21_NULL == s21_strchr("udioxX", specifier) ? 0 : 1;
    int isFloat = S21_NULL == s21_strchr("eEfgG", specifier) ? 0 : 1;
    int isPointer = S21_NULL == s21_strchr("p", specifier) ? 0 : 1;
    int isString = S21_NULL == s21_strchr("s", specifier) ? 0 : 1;
    int isChar = S21_NULL == s21_strchr("c", specifier) ? 0 : 1;
    int isN = S21_NULL == s21_strchr("n", specifier) ? 0 : 1;

    flag *= s21_are_non_sscanf_fields_default_s(tokenPtr);

    if (tokenPtr->WIDTH != WIDTH_DEFAULT) {
        flag *= tokenPtr->WIDTH > 0 ? 1 : 0;  // для всех
    }
    if (isInteger && flag) {
        if (tokenPtr->LENGTH != LENGTH_DEFAULT)
            flag *= S21_NULL == s21_strchr("hl", tokenPtr->LENGTH) ? 0 : 1;
    } else if (isFloat && flag) {
        if (tokenPtr->LENGTH != LENGTH_DEFAULT)
            flag *= S21_NULL == s21_strchr("Ll", tokenPtr->LENGTH) ? 0 : 1;
    } else if (isPointer && flag) {
        flag *= tokenPtr->LENGTH == LENGTH_DEFAULT ? 1 : 0;
    } else if (isString && flag) {
        flag *= tokenPtr->LENGTH == LENGTH_DEFAULT ? 1 : 0;
    } else if (isChar && flag) {
        flag *= tokenPtr->LENGTH == LENGTH_DEFAULT ? 1 : 0;
    } else if (isN && flag) {
        flag *= tokenPtr->WIDTH == WIDTH_DEFAULT ? 1 : 0;
        flag *= tokenPtr->PRECISION == PRECISION_DEFAULT ? 1 : 0;
    } else {
        flag = 0;  // wrong specifier
    }
    return flag;
}

int s21_are_non_sscanf_fields_default_s(Token *tokenPtr) {
    int flag = 1;
    flag *= tokenPtr->flag.plus == 0 ? 1 : 0;
    flag *= tokenPtr->flag.minus == 0 ? 1 : 0;
    flag *= tokenPtr->flag.space == 0 ? 1 : 0;
    flag *= tokenPtr->flag.sharp == 0 ? 1 : 0;
    flag *= tokenPtr->PRECISION == 1 ? 1 : 0;
    return flag;
}

int s21_call_writing_func_s(Token *tokenPtr, const char **strPtr, va_list argsPtr, int *count_symb) {
    int flag = 1;
    if (tokenPtr->specifier == 'c') {
        flag = s21_write_to_char_s(tokenPtr, strPtr, argsPtr, count_symb);
    } else if (s21_strchr("idxXuop", tokenPtr->specifier)) {
        flag = s21_write_to_int_s(tokenPtr, strPtr, argsPtr, count_symb);
    } else if (s21_strchr("feEgG", tokenPtr->specifier)) {
        flag = s21_write_to_float_s(tokenPtr, strPtr, argsPtr, count_symb);
    } else if (tokenPtr->specifier == 's') {
        flag = s21_write_to_str_s(tokenPtr, strPtr, argsPtr, count_symb);
    } else if (tokenPtr->specifier == 'n') {
        flag = s21_write_to_numb_s(tokenPtr, argsPtr, count_symb);
    }
    return flag;
}

int s21_write_to_int_s(Token *tokenPtr, const char **strPtr, va_list argsPtr, int *count_symb) {
    while (**strPtr == ' ') {
        (*strPtr)++;
    }
    if (**strPtr == '+' || **strPtr == '-') {     // вдруг пользователь ввел -+ подряд
        if (S21_NULL == s21_strchr("0123456789aAbBcCdDeEfF", *(*strPtr + 1))) {
            return 0;
        }
    } else if (S21_NULL == s21_strchr("0123456789aAbBcCdDeEfF", **strPtr)) {
        return 0;
    }
    int flag = 1;
    char symbols8[] = "01234567";
    char symbols10[] = "0123456789";
    char symbols16[] = "0123456789aAbBcCdDeEfF";

    char *chosenSymbols;
    if (tokenPtr->specifier == 'i')
        chosenSymbols = symbols10;
    if (tokenPtr->specifier == 'd')
        chosenSymbols = symbols10;
    if (tokenPtr->specifier == 'u')
        chosenSymbols = symbols10;
    if (tokenPtr->specifier == 'x' || tokenPtr->specifier == 'X')
        chosenSymbols = symbols16;
    if (tokenPtr->specifier == 'o')
        chosenSymbols = symbols8;
    if (tokenPtr->specifier == 'p')
        chosenSymbols = symbols16;

    char buffer[BUFFER_SIZE];
    char *buffPtr = buffer;
    s21_memset(buffer, '\0', BUFFER_SIZE);
    int width = tokenPtr->WIDTH;
    int WidthIsDefault = width == WIDTH_DEFAULT ? 1 : 0;
    char *tmpPtr = (char *)*strPtr;
    if (*tmpPtr == '+') {
        tmpPtr++;
        if (!WidthIsDefault) {
            width--;
        }
        (*count_symb)++;
    }
    int isThereSign = 0;
    if (*tmpPtr == '-') {
        isThereSign = 1;
        buffer[0] = '-';
        buffPtr++;
        tmpPtr++;
        if (!WidthIsDefault) width--;
    }
    if (*tmpPtr == '0' && (tokenPtr->specifier == 'i' || tokenPtr->specifier == 'p')) {
        // 8 or 16
        int was_i_specifier = 0;
        if (tokenPtr->specifier == 'i') {
            was_i_specifier = 1;
            tokenPtr->specifier = 'o';
            chosenSymbols = symbols8;
        }
        tmpPtr++;
        (*count_symb)++;
        if (!WidthIsDefault) {
            width--;
        }
        if (tokenPtr->specifier == 'o') {     // width == 0 &&
            if (isThereSign) {
                buffer[1] = '0';
            } else {
                buffer[0] = '0';
            }
        }

        if (*tmpPtr == 'x' || *tmpPtr == 'X') {
            if (tokenPtr->specifier == 'i' || was_i_specifier) {
                tokenPtr->specifier = 'X';
                chosenSymbols = symbols16;
            }
            if (!WidthIsDefault) {
                width--;
            }
            tmpPtr++;
            (*count_symb)++;
        }
    }
    int flag2 = 1;
    if (!(width == 0 && tokenPtr->specifier == 'o')) {
        flag = S21_NULL == s21_get_digits_in_str_s(buffPtr, tmpPtr, chosenSymbols, width) ? 0 : 1;
        if (flag) {
            (tmpPtr) += s21_strlen(buffPtr);
        } else {
            flag = s21_put_int_s(buffer, tokenPtr, argsPtr, count_symb);
            flag2 = 0;
        }
    }
    if (flag && flag2) {
        flag = s21_put_int_s(buffer, tokenPtr, argsPtr, count_symb);
    }
    (*strPtr) += tmpPtr - *strPtr;
    return flag;
}

int s21_put_int_s(char *buffer, Token *tokenPtr, va_list argsPtr, int *count_symb) {
    int flag = 1;
    if (!(*buffer)) {
        flag = 0;
        return flag;
    }

    long long ll_value;
    *count_symb += s21_strlen(buffer);
    switch (tokenPtr->specifier) {
    case 'p':
        ll_value = s21_cast16_to10_s(buffer);
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, long *)) = (long)ll_value;
        return flag;
        break;
    case 'x':
    case 'X':
        ll_value = s21_cast16_to10_s(buffer);
        break;
    case 'o':
        ll_value = s21_cast8_to10_s(buffer);
        break;
    case 'd':
    case 'i':
    case 'u':
        ll_value = s21_atoi(buffer);
        break;
    default:
        flag = 0;
        break;
    }
    // запись в аргумент
    switch (tokenPtr->LENGTH) {
    case 'h':
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, short *)) = (short)ll_value;
        break;
    case 'l':
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, long *)) = (long)ll_value;
        break;
    default:  // обычный инт
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, int *)) = ll_value;
        break;
    }
    return flag;
}

int s21_write_to_float_s(Token *tokenPtr, const char **strPtr, va_list argsPtr, int *count_symb) {
    while (**strPtr == ' ') {
        (*strPtr)++;
    }
    if (**strPtr == '+' || **strPtr == '-') {
        if (S21_NULL == s21_strchr("0123456789.", *(*strPtr + 1))) {
            return 0;
        }
    }

    int flag = 1;
    char symbolsfloat[] = "0123456789.eE-+";

    char buffer[BUFFER_SIZE] = {0};
    char *buffPtr = buffer;
    int width = tokenPtr->WIDTH;
    int WidthIsDefault = width == WIDTH_DEFAULT ? 1 : 0;
    char *tmpPtr = (char *)*strPtr;

    if (*tmpPtr == '-') {
        buffer[0] = '-';
        buffPtr++;
        tmpPtr++;
        if (!WidthIsDefault) {
            width--;
        }
    }

    flag = S21_NULL == s21_get_digits_in_str_s(buffPtr, tmpPtr, symbolsfloat, width) ? 0 : 1;
    if (flag)
        (tmpPtr) += s21_strlen(buffPtr);
    // if (flag)
        flag = s21_put_float_s(buffer, tokenPtr, argsPtr, count_symb);

    (*strPtr) += tmpPtr - *strPtr;
    return flag;
}

int s21_put_float_s(char *buffer, Token *tokenPtr, va_list argsPtr, int *count_symb) {
    int flag = 1;
    long double ld_value = 0.0;
    *count_symb += s21_strlen(buffer);
    ld_value = s21_atof_s(buffer);
    // запись в аргумент
    switch (tokenPtr->LENGTH) {
    case 'l':
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, double *)) = (double)ld_value;
        break;
    case 'L':
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, long double *)) = ld_value;
        break;
    default:  // обычный float
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, float *)) = (float)ld_value;
        break;
    }
    return flag;
}

int s21_write_to_char_s(Token *tokenPtr, const char **strPtr, va_list argsPtr, int *count_symb) {
    int flag = 1;
    char buffer[BUFFER_SIZE] = {0};
    if (tokenPtr->WIDTH == WIDTH_DEFAULT) {
        tokenPtr->WIDTH = 1;
    }
    int width = tokenPtr->WIDTH;
    int count = 0;
    while (**strPtr != '\0' && width > 0) {
        buffer[count] = **strPtr;
        (*strPtr)++;
        count++;
        width--;
    }
    flag = s21_put_char_s(buffer, tokenPtr, argsPtr, count_symb);
    return flag;
}

int s21_write_to_str_s(Token *tokenPtr, const char **strPtr, va_list argsPtr, int *count_symb) {
    int flag = 1;
    char buffer[BUFFER_SIZE] = {0};
    int width = tokenPtr->WIDTH;
    int count = 0;
    int WidthIsDefault = width == WIDTH_DEFAULT ? 1 : 0;

    while (**strPtr == ' ') {
        (*strPtr)++;
    }

    while (**strPtr != ' ' && **strPtr != '\0' && **strPtr != '\t' && **strPtr != '\n') {
        if (width == 0) break;
        buffer[count] = **strPtr;
        (*strPtr)++;
        count++;
        if (!WidthIsDefault) width--;
    }

    flag = s21_put_char_s(buffer, tokenPtr, argsPtr, count_symb);
    return flag;
}

int s21_put_char_s(char *buffer, Token *tokenPtr, va_list argsPtr, int *count_symb) {
    int allisGood = 1;
    int i = 0;
    *count_symb += s21_strlen(buffer);
    char *chPtr = va_arg(argsPtr, char *);
    while (buffer[i]) {
        if (!(tokenPtr->ignoreAssign))
            *(chPtr) = buffer[i];
        chPtr++;
        i++;
    }
    if (tokenPtr->specifier == 's') {
        *chPtr = '\0';
    }
    return allisGood;
}

int s21_write_to_numb_s(Token *tokenPtr, va_list argsPtr, int *count_symb) {
    int flag = 1;
    switch (tokenPtr->LENGTH) {
    case 'h':
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, short *)) = (short)(*count_symb);
        break;
    case 'l':
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, long *)) = (long)(*count_symb);
        break;
    default:  // обычный инт
        if (!(tokenPtr->ignoreAssign))
            *(va_arg(argsPtr, int *)) = (*count_symb);
        break;
    }
    return flag;
}

double s21_atof_s(char *buffer) { return atof(buffer); }

int s21_is_specifier_s(char symb) {
    int isSpecifier;
    int symbNumber = symb - '\0';
    if (s21_strchr("cdieEfgGosuxXpn", symbNumber) != S21_NULL)
        isSpecifier = 1;
    else
        isSpecifier = 0;
    return isSpecifier;
}

void s21_init_struct_s(Token *x) {
    x->flag.plus = 0;
    x->flag.minus = 0;
    x->flag.space = 0;
    x->flag.sharp = 0;
    x->flag.zero = 0;
    x->LENGTH = LENGTH_DEFAULT;
    x->PRECISION = PRECISION_DEFAULT;
    x->WIDTH = WIDTH_DEFAULT;
    x->specifier = '0';
    x->ignoreAssign = 0;
}

int s21_is_flag_s(const char symb) {
    int isFlag;
    int symbNumber = symb - '\0';
    if (s21_strchr("+- #0", symbNumber) != S21_NULL)
        isFlag = 1;
    else
        isFlag = 0;
    return isFlag;
}

void s21_set_flag_s(const char symb, Token *token) {
    if (symb == '+') {
        (*token).flag.plus = 1;
    } else if (symb == '-') {
        (*token).flag.minus = 1;
    } else if (symb == ' ') {
        (*token).flag.space = 1;
    } else if (symb == '#') {
        (*token).flag.sharp = 1;
    } else if (symb == '0') {
        (*token).flag.zero = 1;
    }
}

char *s21_get_digits_in_str_s(char *dest, const char *src, char *digitSymbols, int n) {
    int thereIsDigit = 0;
    int i;
    if (n == -1) {
        n = s21_strlen(src);
    }
    for (i = 0;  i < n && s21_strchr(digitSymbols, src[i]) != S21_NULL; i++) {
        dest[i] = src[i];
        thereIsDigit = 1;
    }
    if (thereIsDigit) {
        dest[i] = '\0';
        return dest;
    } else {
        return S21_NULL;
    }
}

int s21_set_field_s(const char **formatPtr, int *fieldPtr) {
    int allisGoodFlag = 1;
    if (isdigit(**formatPtr)) {
        char buffer[BUFFER_SIZE] = "";
        int getAllDigits = -1;
        s21_get_digits_in_str_s(buffer, *formatPtr, "0123456789", getAllDigits);
        *fieldPtr = s21_atoi(buffer);
        *formatPtr += s21_strlen(buffer);
    } else {
        allisGoodFlag = 0;
    }
    return allisGoodFlag;
}

int s21_cast8_to10_s(char *digitsStr) {
    int value = s21_atoi(digitsStr);
    int sign = value > 0 ? 1 : -1;
    value = value > 0 ? value : -value;                                                         // module
    int sum = 0;
    for (int count = 0; value > 0; count++) {
        int remainer = 0;
        remainer = value % 10;
        sum += remainer * pow(8, count);
        value = (value - remainer) / 10;
    }
    return sign * sum;
}

unsigned long int s21_cast16_to10_s(char *digitsStr) {
    int h = 1;
    if (digitsStr[0] == '-') {
        digitsStr++;
        h *= -1;
    }
    int bit_number = s21_strlen(digitsStr) - 1;
    unsigned long int sum = 0;
    for (int i = 0; bit_number >= 0; i++) {
        char str[2] = {0};
        str[0] = digitsStr[i];
        char *ptr = s21_to_upper(str);
        char symb = *ptr;
        free(ptr);
        sum += ((symb >= 'A') ? 10 + symb - 'A' : symb - '0') * pow(16, bit_number);
        bit_number--;
    }

    return sum * h;
}

int s21_is_length_s(const char symb) {
    int isFlag;
    int symbNumber = symb - '\0';
    if (s21_strchr("hlL", symbNumber) != S21_NULL)
        isFlag = 1;
    else
        isFlag = 0;
    return isFlag;
}
