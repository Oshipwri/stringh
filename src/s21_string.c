#include "s21_string.h"

#if defined(__APPLE__)
#define MAX_ERRLIST 107
#define MIN_ERRLIST -1
#define error "Unknown error: "

static const char *maserror[] = {
  "Undefined error: 0",
  "Operation not permitted",
  "No such file or directory",
  "No such process",
  "Interrupted system call",
  "Input/output error",
  "Device not configured",
  "Argument list too long",
  "Exec format error",
  "Bad file descriptor",
  "No child processes",
  "Resource deadlock avoided",
  "Cannot allocate memory",
  "Permission denied",
  "Bad address",
  "Block device required",
  "Resource busy",
  "File exists",
  "Cross-device link",
  "Operation not supported by device",
  "Not a directory",
  "Is a directory",
  "Invalid argument",
  "Too many open files in system",
  "Too many open files",
  "Inappropriate ioctl for device",
  "Text file busy",
  "File too large",
  "No space left on device",
  "Illegal seek",
  "Read-only file system",
  "Too many links",
  "Broken pipe",
  "Numerical argument out of domain",
  "Result too large",
  "Resource temporarily unavailable",
  "Operation now in progress",
  "Operation already in progress",
  "Socket operation on non-socket",
  "Destination address required",
  "Message too long",
  "Protocol wrong type for socket",
  "Protocol not available",
  "Protocol not supported",
  "Socket type not supported",
  "Operation not supported",
  "Protocol family not supported",
  "Address family not supported by protocol family",
  "Address already in use",
  "Can't assign requested address",
  "Network is down",
  "Network is unreachable",
  "Network dropped connection on reset",
  "Software caused connection abort",
  "Connection reset by peer",
  "No buffer space available",
  "Socket is already connected",
  "Socket is not connected",
  "Can't send after socket shutdown",
  "Too many references: can't splice",
  "Operation timed out",
  "Connection refused",
  "Too many levels of symbolic links",
  "File name too long",
  "Host is down",
  "No route to host",
  "Directory not empty",
  "Too many processes",
  "Too many users",
  "Disc quota exceeded",
  "Stale NFS file handle",
  "Too many levels of remote in path",
  "RPC struct is bad",
  "RPC version wrong",
  "RPC prog. not avail",
  "Program version wrong",
  "Bad procedure for program",
  "No locks available",
  "Function not implemented",
  "Inappropriate file type or format",
  "Authentication error",
  "Need authenticator",
  "Device power is off",
  "Device error",
  "Value too large to be stored in data type",
  "Bad executable (or shared library)",
  "Bad CPU type in executable",
  "Shared library version mismatch",
  "Malformed Mach-o file",
  "Operation canceled",
  "Identifier removed",
  "No message of desired type",
  "Illegal byte sequence",
  "Attribute not found",
  "Bad message",
  "EMULTIHOP (Reserved)",
  "No message available on STREAM",
  "ENOLINK (Reserved)",
  "No STREAM resources",
  "Not a STREAM",
  "Protocol error",
  "STREAM ioctl timeout",
  "Operation not supported on socket",
  "Policy not found",
  "State not recoverable",
  "Previous owner died",
  "Interface output queue is full"
};

#elif defined(__linux__)
#define MAX_ERRLIST 134
#define MIN_ERRLIST -1
#define error  "Unknown error "

static const char *maserror[] = {
    "Success",
    "Operation not permitted",
    "No such file or directory",
    "No such process",
    "Interrupted system call",
    "Input/output error",
    "No such device or address",
    "Argument list too long",
    "Exec format error",
    "Bad file descriptor",
    "No child processes",
    "Resource temporarily unavailable",
    "Cannot allocate memory",
    "Permission denied",
    "Bad address",
    "Block device required",
    "Device or resource busy",
    "File exists",
    "Invalid cross-device link",
    "No such device",
    "Not a directory",
    "Is a directory",
    "Invalid argument",
    "Too many open files in system",
    "Too many open files",
    "Inappropriate ioctl for device",
    "Text file busy",
    "File too large",
    "No space left on device",
    "Illegal seek",
    "Read-only file system",
    "Too many links",
    "Broken pipe",
    "Numerical argument out of domain",
    "Numerical result out of range",
    "Resource deadlock avoided",
    "File name too long",
    "No locks available",
    "Function not implemented",
    "Directory not empty",
    "Too many levels of symbolic links",
    "Unknown error 41",
    "No message of desired type",
    "Identifier removed",
    "Channel number out of range",
    "Level 2 not synchronized",
    "Level 3 halted",
    "Level 3 reset",
    "Link number out of range",
    "Protocol driver not attached",
    "No CSI structure available",
    "Level 2 halted",
    "Invalid exchange",
    "Invalid request descriptor",
    "Exchange full",
    "No anode",
    "Invalid request code",
    "Invalid slot",
    "Unknown error 58",
    "Bad font file format",
    "Device not a stream",
    "No data available",
    "Timer expired",
    "Out of streams resources",
    "Machine is not on the network",
    "Package not installed",
    "Object is remote",
    "Link has been severed",
    "Advertise error",
    "Srmount error",
    "Communication error on send",
    "Protocol error",
    "Multihop attempted",
    "RFS specific error",
    "Bad message",
    "Value too large for defined data type",
    "Name not unique on network",
    "File descriptor in bad state",
    "Remote address changed",
    "Can not access a needed shared library",
    "Accessing a corrupted shared library",
    ".lib section in a.out corrupted",
    "Attempting to link in too many shared libraries",
    "Cannot exec a shared library directly",
    "Invalid or incomplete multibyte or wide character",
    "Interrupted system call should be restarted",
    "Streams pipe error",
    "Too many users",
    "Socket operation on non-socket",
    "Destination address required",
    "Message too long",
    "Protocol wrong type for socket",
    "Protocol not available",
    "Protocol not supported",
    "Socket type not supported",
    "Operation not supported",
    "Protocol family not supported",
    "Address family not supported by protocol",
    "Address already in use",
    "Cannot assign requested address",
    "Network is down",
    "Network is unreachable",
    "Network dropped connection on reset",
    "Software caused connection abort",
    "Connection reset by peer",
    "No buffer space available",
    "Transport endpoint is already connected",
    "Transport endpoint is not connected",
    "Cannot send after transport endpoint shutdown",
    "Too many references: cannot splice",
    "Connection timed out",
    "Connection refused",
    "Host is down",
    "No route to host",
    "Operation already in progress",
    "Operation now in progress",
    "Stale file handle",
    "Structure needs cleaning",
    "Not a XENIX named type file",
    "No XENIX semaphores available",
    "Is a named type file",
    "Remote I/O error",
    "Disk quota exceeded",
    "No medium found",
    "Wrong medium type",
    "Operation canceled",
    "Required key not available",
    "Key has expired",
    "Key has been revoked",
    "Key was rejected by service",
    "Owner died",
    "State not recoverable",
    "Operation not possible due to RF-kill",
    "Memory page has hardware error"
};
#endif

void *s21_memchr(const void *str, int c, s21_size_t n) {
    char *s1 = (char *)str;
    int flag = 0;
    char *buf = S21_NULL;
    for (s21_size_t i = 0; i < n; i++) {
        if (flag == 0) {
            int man = s1[i];
            if (man == c) {
                buf = (char *)s1 + i;
                flag = 1;
            }
        }
    }
    return flag == 0 ? S21_NULL : buf;
}

int s21_memcmp(const void *str1, const void *str2, s21_size_t n) {
    const unsigned char *s1;
    const unsigned char *s2;
    s1 = str1;
    s2 = str2;
    char count = 0;
    for (s21_size_t i=0; i < n; i++) {
        if (s1[i] != s2[i]) {
            count = s1[i] - s2[i];
            break;
        }
    }
    return count;
}

void *s21_memcpy(void *dest, const void *src, s21_size_t num) {
    unsigned char *temp_src = (unsigned char*)src;
    unsigned char *temp_dest = (unsigned char*)dest;
    for (s21_size_t i = 0; i < num; i++) {
      temp_dest[i] = temp_src[i];
    }
    return temp_dest;
}

void *s21_memmove(void *dest, const void *src, s21_size_t n) {
    char *s;
    char *d;
    s = (char *)src + n - 1;
    d = (char *)dest + n - 1;
    for (s21_size_t i = 0; n != 0; i--, n--) {
        d[i] = s[i];
    }
    return dest;
}

char *s21_strpbrk(const char *str1, const char *str2) {
    char *s1;
    char *elem;
    int flag = 0;
    s1 = (char*)str1;
    while (*s1) {
        char *s2 = (char*)str2;
        while (*s2) {
            if (*s1 == *s2) {
                flag = 1;
                elem = s1;
                break;
            }
            ++s2;
        }
        if (flag) {
            break;
        }
    ++s1;
    }
    if (!flag) {
        elem = S21_NULL;
    }
    return elem;
}

void *s21_memset(void *str, int c, s21_size_t n) {
    char *str1 = (char*)str;
    char symb = (char)c;
    for (s21_size_t i = 0; i < n; i++) {
        str1[i] = symb;
    }
    return str;
}

s21_size_t s21_strlen(const char *str) {
    s21_size_t len = 0;
    for (; *str; str++, len ++) {}
    return len;
}

char *s21_strcat(char *dest, const char *src) {
    for (char *p = dest + s21_strlen(dest); (*p++ = *src++);) {}
    return dest;
}

char *s21_strncat(char *dest, const char *src, s21_size_t n) {
    for (char *p = dest + s21_strlen(dest); n != 0 && (*p++ = *src++); n--) {}
    return dest;
}

int s21_strcmp(const char *str1, const char *str2) {
    for ( ; *str1 == *str2; ++str1, ++str2)
        if ( *str2 == '\0' ) {
            break;
        }
    return (unsigned char) *str1 - (unsigned char) *str2;
}

s21_size_t s21_strspn(const char *str1, const char *str2) {
    char const *s1;
    s21_size_t count = 0;
    s1 = str1;
    while (*s1) {
        int c = (int)(*s1);
        if (s21_strchr(str2, c) != S21_NULL) {
            count++;
        } else {
            break;
        }
        s1++;
    }
    return count;
}

s21_size_t s21_strcspn(const char *str1, const char *str2) {
    char const *s1;
    int count = 0;
    int flag = 0;
    s1 = str1;
    while (*s1 && flag == 0) {
        char const *s2 = str2;
        while (*s2 && flag == 0) {
            if (*s1 == *s2) {
                flag = 1;
            }
            s2++;
        }
        if (flag) {
            break;
        }
        s1++;
        count++;
    }
    return count;
}

char *s21_strchr(const char *str, int c) {
    char k = c;
    int flag = 1;
    char *man = S21_NULL;
    if (k == '\0') {
        man = "";
    } else {
        for (int i = 0; str[i] != '\0'; i++) {
            if (str[i] == k) {
                man = (char *)str + i;
                break;
            }
        }
    }
    if (man == S21_NULL) {
        flag = 0;
    }
    return flag == 0 ? S21_NULL : man;
}

int s21_strncmp(const char* str1, const char* str2, s21_size_t n) {
    int nam = 0;
    char* man = (char *)str1 + n;
    while (nam == 0 && (*str1 || *str2) && str1 != man) {
        nam = (*str1++) - (*str2++);
    }
    return nam;
}

char *s21_strcpy(char *dest, const char *src) {
    for (s21_size_t i = 0; src[i] != 0; i++) {
        dest[i] = src[i];
        if (src[i + 1] == 0) dest[i + 1] = 0;
    }
    return dest;
}

char *s21_strncpy(char *dest, const char *src, s21_size_t n) {
    s21_size_t srcLen = s21_strlen(src);
    for (s21_size_t i = 0; i < n; i++) {
        if (i <= srcLen) {
            dest[i] = src[i];
        } else {
            dest[i] = 0;
        }
    }
    return dest;
}

char *s21_strrchr(const char *str, int c) {
    char k = c;
    char *res = S21_NULL;
        for (; *str != '\0'; str++) {
            if (*str == k) {
                res = (char *)str;
            }
        }
    if (res == S21_NULL) {
        res = (char *)str;
    }
    return *res == c ? (char *)res : S21_NULL;
}

char *s21_strstr(const char *haystack, const char *needle) {
    char *str = (char *)haystack;
    char *pointer = S21_NULL;
    int len = s21_strlen(needle) - 1;
        for (int i = 0; str[i] != '\0' && pointer == S21_NULL; i++) {
            int counter = 0, flag = 1;
            while ((counter <= len) && flag) {
                flag = str[i + counter] == needle[counter];
                counter++;
            }
            if (flag && (counter > len)) {
                pointer = &(str[i]);
            }
        }
    return pointer;
}

void *s21_to_upper(const char *str) {
    char *our_str1 = S21_NULL;
    if (str) {
        our_str1 = (char*)malloc((s21_strlen(str) + 1)*sizeof(char));
    }
    if (our_str1) {
        s21_strncpy(our_str1, str, s21_strlen(str) + 1);
        for (char *i = our_str1; *i; i++) {
            if (*i >= 'a' && *i <= 'z') {
                *i -= 32;
            }
        }
    }
    return (void *)our_str1;
}

void *s21_to_lower(const char *str) {
    char *our_str2 = S21_NULL;
    if (str) {
        our_str2 = (char*)malloc((s21_strlen(str) + 1)*sizeof(char));
    }
    if (our_str2) {
        s21_strncpy(our_str2, str, s21_strlen(str) + 1);
        for (char *i = our_str2; *i; i++) {
            if (*i >= 'A' && *i <= 'Z') {
                *i += 32;
            }
        }
    }
    return (void *)our_str2;
}

char *s21_strtok(char *str, const char *delim) {
    char *returnPtr = S21_NULL;
    static char* chPtr = S21_NULL;
    static char token[100] = "";
    int isThereToken = 0;
    if (str != S21_NULL && *str != '\0') {
        chPtr = str;
    } else if ((str == S21_NULL || *str == '\0') && (chPtr == S21_NULL /*|| *chPtr == '\0'*/)) {
        chPtr = S21_NULL;
        return S21_NULL;
    }
    int delimCount = 0;
    for (s21_size_t i = 0; ; i++) {
        char* isDelim = s21_strchr(delim, (int)chPtr[i]);
        if (isDelim != S21_NULL && *isDelim == '\0') isDelim = S21_NULL;
        if (isDelim == S21_NULL && chPtr[i] != 0) {
            isThereToken = 1;
            token[i - delimCount] = chPtr[i];
        } else if (isDelim && isThereToken) {
            chPtr = &(chPtr[i + 1]);
            token[i - delimCount] = '\0';
            returnPtr = token;
            break;
        } else if (isDelim && !isThereToken) {
            delimCount++;
        } else if (chPtr[i] == '\0' && isThereToken) {
            chPtr = &(chPtr[i]);
            token[i - delimCount] = '\0';
            chPtr = S21_NULL;
            returnPtr = token;
            break;
        } else if (chPtr[i] == '\0' && !isThereToken) {
            chPtr = S21_NULL;
            returnPtr = S21_NULL;
            break;
        } else {
            returnPtr = S21_NULL;
            break;
        }
    }
    return returnPtr;
}


void *s21_insert(const char *src, const char *str, s21_size_t start_index) {
    int flag = 0;
    char *man = S21_NULL;
    if (src != S21_NULL) {
        s21_size_t head = s21_strlen(src);
        s21_size_t minor = s21_strlen(str);
        s21_size_t sum = head + minor;
        if (head >= start_index) {
            s21_size_t am = 0;
            man = (char *)malloc((sum + 1) * sizeof(char));
            for (s21_size_t i = 0; i < sum; i++) {
                if (i == start_index) {
                    for (s21_size_t j = 0; j < minor; j++) {
                        man[am] = str[j];
                        am++;
                    }
                }
                man[am] = src[i];
                am++;
            }
        } else {
            flag = 1;
        }
    } else {
        flag = 1;
    }
    return flag == 0 ? man : S21_NULL;
}

void s21_invert(char *str, int ern) {
  int a = 0;
  int arr = ern;
  if (arr < 0) {
    arr *= -1;
  }
  while (arr > 0) {
    str[a] = arr % 10 + '0';
    a++;
    arr /= 10;
  }
  if (ern < 0) {
    str[a] = '-';
    a++;
  }
  str[a] = '\0';
  int am = s21_strlen(str) - 1;
  for (int i = 0; i < am; i++, am--) {
    char nan = str[i];
    str[i] = str[am];
    str[am] = nan;
  }
}

char *s21_strerror(int errnum) {
  char *tim = S21_NULL;
  if (errnum > MIN_ERRLIST && errnum < MAX_ERRLIST) {
    tim = (char *)maserror[errnum];
  } else {
    static char man[33] = {0};
    static char aray[33] = error;
    s21_invert(man, errnum);
    s21_strcat(aray, man);
    tim = aray;
  }
  return tim;
}

void *s21_trim(const char *src, const char *trim_chars) {
    char *mani = (char *)src;
    char *minor = (char *)trim_chars;
    char *arr;
    int man = 1;
    if (minor == S21_NULL) {
        trim_chars = " ";
        minor = (char *)trim_chars;
    }
    if (mani == S21_NULL) {
        man = 0;
    }
    if (man) {
        s21_size_t head = s21_strlen(src);
        s21_size_t nohead = s21_strlen(trim_chars);
        arr = (char *)malloc((head + 1) * sizeof(char));
        int arr_i = 0;
        int st_i = s21_start_i(mani, minor, head, nohead);
        int en_i = s21_end_i(mani, minor, head, nohead);
        for (int i = st_i; i < en_i; i++) {
            arr[arr_i] = src[i];
            arr_i++;
        }
        arr[arr_i] = '\0';
    }
    return man == 0 ? S21_NULL : (char *)arr;
}

int s21_start_i(const char *mani, const char *minor, s21_size_t head, s21_size_t nohead) {
    int start_i = 0;
    for (s21_size_t i = 0; i < head; i++) {
        int flag1 = 0;
        for (s21_size_t j = 0; j < nohead; j++) {
            if (mani[i] == minor[j]) {
                flag1 = 1;
                break;
            }
        }
        if (flag1 == 1) {
        start_i++;
        } else {
        break;
        }
    }
    return start_i;
}

int s21_end_i(const char *mani, const char *minor, s21_size_t head, s21_size_t nohead) {
    int end_i = head;
    for (int i = head - 1; i > -1; i--) {
        int flag2 = 0;
        for (s21_size_t j = 0; j < nohead; j++) {
            if (mani[i] == minor[j]) {
                flag2 = 1;
                break;
            }
        }
        if (flag2 == 1) {
        end_i--;
        } else {
        break;
        }
    }
    return end_i;
}

long long s21_atoi(char *x) {
    int sign = 1;
    if (x[0] == '-') {
        x++;
        sign *= -1;
    }
    long long numb = 0;
    for (int i = 0; x[i] >= '0' && x[i] <= '9'; i++) {
        numb *= 10;
        numb += x[i] - '0';
    }
    return numb * sign;
}
