#ifndef SRC_S21_STRING_H_
#define SRC_S21_STRING_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <locale.h>
#include <math.h>
#include <stdarg.h>

typedef long unsigned s21_size_t;
#define S21_NULL ((void *)0)

struct token {
    struct flag {
        int plus;
        int minus;
        int space;
        int sharp;
        int zero;
    } flag;
    int WIDTH;
    int PRECISION;
    char LENGTH;
    char specifier;
    int ignoreAssign;
    int fact_width;
};
typedef struct token Token;
typedef struct flag FLAG;

void *s21_memchr(const void *str, int c, s21_size_t n);
int s21_memcmp(const void *str1, const void *str2, s21_size_t n);
void *s21_memcpy(void *dest, const void *src, s21_size_t num);
void *s21_memmove(void *dest, const void *src, s21_size_t n);
void *s21_memset(void *str, int c, s21_size_t n);
s21_size_t s21_strlen(const char *str);
char *s21_strcat(char *dest, const char *src);
char *s21_strncat(char *dest, const char *src, s21_size_t n);
int s21_strcmp(const char *str1, const char *str2);
s21_size_t s21_strspn(const char *str1, const char *str2);
s21_size_t s21_strcspn(const char *str1, const char *str2);
char *s21_strpbrk(const char *str1, const char *str2);
char *s21_strchr(const char *str, int c);
int s21_strncmp(const char *str1, const char *str2, s21_size_t n);
char *s21_strcpy(char *dest, const char *src);
char *s21_strncpy(char *dest, const char *src, s21_size_t n);
char *s21_strrchr(const char *str, int c);
char *s21_strstr(const char *haystack, const char *needle);
char *s21_strtok(char *str, const char *delim);
void *s21_to_upper(const char *str);
void *s21_to_lower(const char *str);
void *s21_insert(const char *src, const char *str, s21_size_t start_index);
char *s21_strerror(int errnum);
void s21_invert(char *str, int ern);
void *s21_trim(const char *src, const char *trim_chars);
int s21_start_i(const char *mani, const char *minor, s21_size_t head, s21_size_t nohead);
int s21_end_i(const char *mani, const char *minor, s21_size_t head, s21_size_t nohead);

int s21_sscanf(const char *str, const char *format, ...);
int s21_sprintf(char *str, const char *format, ...);

long long s21_atoi(char *x);

#endif  // SRC_S21_STRING_H_
