#include "s21_string.h"
#include <stdlib.h>

#define BUFFER_SIZE 300
#define WIDTH_DEFAULT -1
#define LENGTH_DEFAULT ' '
#define PRECISION_DEFAULT -1

int s21_start_processing_p(char *str, const char *format, va_list argsPtr);
int s21_start_processing_p(char *outStr, const char *format, va_list argsPtr);
int s21_format_parsing_p(const char **formatPtr, Token *tokenPtr, va_list argsPtr);
int s21_is_format_correct_p(Token *tokenPtr);
int s21_are_non_fields_default_p(Token *tokenPtr);
int s21_call_writing_func_p(Token *tokenPtr, char *buffPtr, va_list argsPtr);
int s21_is_flag_p(const char symb);
void s21_set_flag_p(const char symb, Token *token);
int s21_set_field_p(const char **formatPtr, int *fieldPtr, va_list argsPtr, char mode, Token *tokenPtr);
int s21_is_specifier_p(char symb);
int s21_is_length_p(const char symb);
char *s21_get_digits_in_str_p(char *dest, const char *src);
void s21_init_struct_p(Token *x);
int s21_write_to_float_p(Token *tokenPtr, char *buffPtr, va_list argsPtr);
int s21_write_to_char_p(Token *tokenPtr, char *strPtr, va_list argsPtr);
int s21_write_to_str_p(Token *tokenPtr, char *buffer, va_list argsPtr);
int s21_write_to_int_p(Token *tokenPtr, char *buffer, va_list argsPtr);
void s21_itoa_p(long long num, char *str);

int s21_sprintf(char *outStr, const char *format, ...) {
    int successCount;
    va_list argsPtr;
    va_start(argsPtr, format);
    if (format != S21_NULL && outStr != S21_NULL) {
        successCount = s21_start_processing_p(outStr, format, argsPtr);
    } else {
        successCount = 0;
    }
    va_end(argsPtr);
    return successCount;
}

int s21_start_processing_p(char *outStr, const char *format, va_list argsPtr) {
    int successCount = 0;
    // int CountSymbls = 0;
    Token token;
    char *outStrCursor = outStr;
    char *beginStrCursor = outStr;
    while (*format) {
        s21_init_struct_p(&token);
        if (*format != '%') {
            *outStrCursor = *format;
            outStrCursor++;
            format++;
        } else {
            char buffer[BUFFER_SIZE] = "";
            format++;
            int allisGood = s21_format_parsing_p(&format, &token, argsPtr);
            if (allisGood == -1) {
                *outStrCursor = '%';
                outStrCursor++;
                format++;
                continue;
            }
            if (allisGood) allisGood = s21_is_format_correct_p(&token);
            if (allisGood) allisGood = s21_call_writing_func_p(&token, buffer, argsPtr);
            if (allisGood) {
                s21_strncpy(outStrCursor, buffer, token.fact_width);
                outStrCursor = outStrCursor + token.fact_width;
                successCount++;
            } else {
                break;
            }
        }
    }
    *outStrCursor = '\0';
    return s21_strlen(beginStrCursor);
}

int s21_format_parsing_p(const char **formatPtr, Token *tokenPtr, va_list argsPtr) {
    int returnValue = 1;
    while (*formatPtr) {
        if (s21_is_flag_p(**formatPtr)) {
            s21_set_flag_p(**formatPtr, tokenPtr);
            (*formatPtr)++;
        } else if (isdigit(**formatPtr) || (**formatPtr) == '*') {
            s21_set_field_p(formatPtr, &tokenPtr->WIDTH, argsPtr, 'w', tokenPtr);
        } else if (**formatPtr == '.') {
            (*formatPtr)++;
            if (isdigit(**formatPtr) || (**formatPtr) == '*') {
                s21_set_field_p(formatPtr, &tokenPtr->PRECISION, argsPtr, 'p', tokenPtr);
            } else {
                tokenPtr->PRECISION = 0;
                continue;
            }
        } else if (s21_is_length_p(**formatPtr)) {
            tokenPtr->LENGTH = **formatPtr;
            (*formatPtr)++;
        } else if (**formatPtr == '%') {
            returnValue = -1;
            break;
        } else if (s21_is_specifier_p(**formatPtr)) {
            tokenPtr->specifier = **formatPtr;
            (*formatPtr)++;
            returnValue = 1;
            break;
        } else {
            returnValue = 0;
            break;
        }
    }
    return returnValue;
}

int s21_is_format_correct_p(Token *tokenPtr) {
    char specifier = tokenPtr->specifier;
    // int width = tokenPtr->WIDTH;
    char length = tokenPtr->LENGTH;
    int precision = tokenPtr->PRECISION;
    int flag = 1;

    int isInteger = S21_NULL == s21_strchr("di", specifier) ? 0 : 1;
    int isUnsigned = S21_NULL == s21_strchr("u", specifier) ? 0 : 1;
    int isString = S21_NULL == s21_strchr("s", specifier) ? 0 : 1;
    int isFloat = S21_NULL == s21_strchr("f", specifier) ? 0 : 1;
    int isChar = S21_NULL == s21_strchr("c", specifier) ? 0 : 1;
    if (!isInteger && !isUnsigned && !isFloat && !isString && !isChar) flag = 0;

    flag *= tokenPtr->flag.sharp == 0 ? 1 : 0;
    flag *= tokenPtr->flag.zero == 0 ? 1 : 0;

    if (isInteger && flag) {
        if (length != LENGTH_DEFAULT) {
            flag *= S21_NULL == s21_strchr("hl", length) ? 0 : 1;
        }
    } else if (isUnsigned && flag) {
            flag *= tokenPtr->flag.plus == 0 ? 1 : 0;
            flag *= tokenPtr->flag.space == 0 ? 1 : 0;
    } else if (isFloat && flag) {
        if (length != LENGTH_DEFAULT) {
            flag *= length == 'l' ? 1 : 0;
        }
    } else if (isString && flag) {
        flag *= length == LENGTH_DEFAULT ? 1 : 0;
        flag *= tokenPtr->flag.plus == 0 ? 1 : 0;
        flag *= tokenPtr->flag.space == 0 ? 1 : 0;
    } else if (isChar && flag) {
        flag *= precision == PRECISION_DEFAULT ? 1 : 0;
        flag *= length == LENGTH_DEFAULT ? 1 : 0;
        flag *= tokenPtr->flag.plus == 0 ? 1 : 0;
        flag *= tokenPtr->flag.space == 0 ? 1 : 0;
    }
    return flag;
}

int s21_call_writing_func_p(Token *tokenPtr, char *buffer, va_list argsPtr) {
    int flag = 1;
    if (tokenPtr->specifier == 'c') {
        flag = s21_write_to_char_p(tokenPtr, buffer, argsPtr);
    } else if (s21_strchr("diu", tokenPtr->specifier)) {
        flag = s21_write_to_int_p(tokenPtr, buffer, argsPtr);
    } else if (tokenPtr->specifier == 's') {
        flag = s21_write_to_str_p(tokenPtr, buffer, argsPtr);
    } else if (tokenPtr->specifier == 'f') {
       s21_write_to_float_p(tokenPtr, buffer, argsPtr);
    } else {
        flag = 0;
    }
    return flag;
}

int s21_is_flag_p(const char symb) {
  int isFlag;
  int symbNumber = symb - '\0';
  if (s21_strchr("+- #0", symbNumber) != S21_NULL)
    isFlag = 1;
  else
    isFlag = 0;
  return isFlag;
}

void s21_set_flag_p(const char symb, Token *token) {
  if (symb == '+') {
    (*token).flag.plus = 1;
  } else if (symb == '-') {
    (*token).flag.minus = 1;
  } else if (symb == ' ') {
    (*token).flag.space = 1;
  }
}

int s21_set_field_p(const char **formatPtr, int *fieldPtr, va_list argsPtr, char mode, Token *tokenPtr) {
    int allisGoodFlag = 1;
    if (isdigit(**formatPtr)) {
        char buffer[BUFFER_SIZE] = "";
        s21_get_digits_in_str_p(buffer, *formatPtr);
        *fieldPtr = s21_atoi(buffer);
        *formatPtr += s21_strlen(buffer);
    } else if (**formatPtr == '*') {
        int value = va_arg(argsPtr, int);
        if (mode == 'w' && value < 0) {
            value *= -1;
            tokenPtr->flag.minus = 1;
        }
        *fieldPtr =  value;
        (*formatPtr)++;
    } else {
        allisGoodFlag = 0;
    }
    return allisGoodFlag;
}

int s21_is_specifier_p(char symb) {
  int isSpecifier;
  int symbNumber = symb - '\0';
  if (s21_strchr("cdieEfgGosuxXpn", symbNumber) != S21_NULL)
    isSpecifier = 1;
  else
    isSpecifier = 0;
  return isSpecifier;
}

int s21_is_length_p(const char symb) {
    int isFlag;
    int symbNumber = symb - '\0';
    if (s21_strchr("hlL", symbNumber) != S21_NULL) {
        isFlag = 1;
    } else {
        isFlag = 0;
    }
    return isFlag;
}

char *s21_get_digits_in_str_p(char *dest, const char *src) {
  char *returnPtr;
  int thereIsDigit = 0;
  size_t i;
  for (i = 0; isdigit(src[i]); i++) {
    dest[i] = src[i];
    thereIsDigit = 1;
  }
  if (thereIsDigit) {
    dest[i] = '\0';
    returnPtr = dest;
  } else {
    returnPtr = S21_NULL;
  }
  return returnPtr;
}

void s21_init_struct_p(Token *x) {
  x->flag.plus = 0;
  x->flag.minus = 0;
  x->flag.space = 0;
  x->flag.sharp = 0;
  x->flag.zero = 0;
  x->LENGTH = LENGTH_DEFAULT;
  x->PRECISION = PRECISION_DEFAULT;
  x->WIDTH = WIDTH_DEFAULT;
  x->specifier = '0';
  x->ignoreAssign = 0;
  x->fact_width = 0;
}

int s21_write_to_char_p(Token *tokenPtr, char *buffer, va_list argsPtr) {
    int flag = 1;
    if (tokenPtr->WIDTH == WIDTH_DEFAULT) {
        tokenPtr->WIDTH = 1;
    }
    int width = tokenPtr->WIDTH;
    char symb = (char)va_arg(argsPtr, int);
    int len = width;
    tokenPtr->fact_width = len;
    int i = 0;
    if (tokenPtr->flag.minus == 1) {
        buffer[i] = symb;
        i++;
        for (; len > 1; len--) {
            buffer[i] = ' ';
            i++;
        }
    } else {
        for (; len > 1; len--) {
            buffer[i] = ' ';
            i++;
        }
        buffer[i] = symb;
    }
    return flag;
}

int s21_write_to_str_p(Token *tokenPtr, char *buffer, va_list argsPtr) {
    char *inStr = (char *)va_arg(argsPtr, char*);
    int precision = tokenPtr->PRECISION;
    int width = tokenPtr->WIDTH;
    int realLength = s21_strlen(inStr);
    int len_to_write = realLength;
    if (precision != PRECISION_DEFAULT) {
        len_to_write = precision < realLength ? precision : realLength;
    }
    tokenPtr->fact_width = len_to_write;
    if (width > len_to_write) {
        tokenPtr->fact_width  = width;
        if (tokenPtr->flag.minus) {
            s21_strncpy(buffer, inStr, len_to_write);
            for (int i = len_to_write; i < width; i++) {
                buffer[i] = ' ';
            }
        } else {
            int i;
            for (i = 0; i < width - len_to_write; i++) {
                buffer[i] = ' ';
            }
            s21_strncpy(buffer + i, inStr, len_to_write);
        }
    } else {
        s21_strncpy(buffer, inStr, len_to_write);
    }
    return 1;
}

int s21_write_to_int_p(Token *tokenPtr, char *buffer, va_list argsPtr) {
    int flag = 1;
    long l_value;
    int i_value;
    unsigned ui_value;
    short sh_value;
    unsigned short ush_value;
    char tmpBuffer[BUFFER_SIZE] = "";
    char length = tokenPtr->LENGTH;
    char specifier = tokenPtr->specifier;

    l_value = va_arg(argsPtr, long);
    if (length == 'l') {
        if (specifier == 'd' || specifier == 'i') {
            s21_itoa_p(l_value, tmpBuffer);
        } else if (specifier == 'u') {
            unsigned long ul_value;
            ul_value = (unsigned long)l_value;
            s21_itoa_p(ul_value, tmpBuffer);
        }
    } else if (length == 'h') {
        if (specifier == 'd' || specifier == 'i') {
            sh_value = (short)l_value;
            s21_itoa_p(sh_value, tmpBuffer);
        } else if (specifier == 'u') {
            ush_value = (unsigned short)l_value;
            s21_itoa_p(ush_value, tmpBuffer);
        }
    } else if (length == LENGTH_DEFAULT) {
        if (specifier == 'd' || specifier == 'i') {
            i_value = (int)l_value;
            s21_itoa_p(i_value, tmpBuffer);
        } else if (specifier == 'u') {
            ui_value = (unsigned int)l_value;
            s21_itoa_p(ui_value, tmpBuffer);
        }
    }
    int sign = tmpBuffer[0] == '-' ? -1:1;
    int only_digits_len  = sign > 0 ? s21_strlen(tmpBuffer) : s21_strlen(tmpBuffer) - 1;
    char lstBuf[BUFFER_SIZE] = "";
    char *Ptr_last_buffer = lstBuf;

    if (tokenPtr->flag.plus) {
        if (sign < 0) {
            lstBuf[0] = '-';
            Ptr_last_buffer++;
        } else {
            lstBuf[0] = '+';
            Ptr_last_buffer++;
        }
    } else if (sign < 0) {
        lstBuf[0] = '-';
        Ptr_last_buffer++;
    }
    if (tokenPtr->PRECISION != PRECISION_DEFAULT) {
        for (int i = tokenPtr->PRECISION - only_digits_len; i > 0; i--) {
            *Ptr_last_buffer = '0';
            Ptr_last_buffer++;
        }
    }
    if ((s21_strcmp(tmpBuffer, "0") == 0) && (tokenPtr->specifier == 'i' || tokenPtr->specifier == 'd')) {
        if (tokenPtr->PRECISION < 0) {
            *Ptr_last_buffer = '0';
            // Ptr_last_buffer++;
        }
        tokenPtr->fact_width = 0;
    } else {
        if (sign < 0) {
            s21_strncpy(Ptr_last_buffer, tmpBuffer+1, only_digits_len);
        } else {
            s21_strncpy(Ptr_last_buffer, tmpBuffer, only_digits_len);
        }
    }
    int fct_l_wrt = tokenPtr->WIDTH > (int)s21_strlen(lstBuf) ? tokenPtr->WIDTH : (int)s21_strlen(lstBuf);
    tokenPtr->fact_width = s21_strlen(lstBuf);
    if (tokenPtr->flag.minus) {
        s21_strcpy(buffer, lstBuf);
        for (int i = s21_strlen(lstBuf); i < fct_l_wrt; i++) {
            buffer[i] = ' ';
            tokenPtr->fact_width++;
        }
    } else {
        int i = 0;
        for (; i < fct_l_wrt - (int)s21_strlen(lstBuf); i++) {
            buffer[i] = ' ';
            tokenPtr->fact_width++;
        }
        s21_strcpy(buffer+i, lstBuf);
    }
    return flag;
}

void s21_itoa_p(long long num, char *str) {
    int a = 0;
    long long arr = num;
    if (arr < 0) {
        arr *= -1;
    } else if (arr == 0) {
        str[a] = '0';
        a++;
    }
    while (arr > 0) {
        str[a] = arr % 10 + '0';
        a++;
        arr /= 10;
    }
    if (num < 0) {
        str[a] = '-';
        a++;
    }
    str[a] = '\0';
    int am = s21_strlen(str) - 1;
    for (int i = 0; i < am; i++, am--) {
        char nan = str[i];
        str[i] = str[am];
        str[am] = nan;
    }
}

int s21_write_to_float_p(Token *tokenPtr, char *buffPtr, va_list argsPtr) {
    int flag = 1;
    char *bufferBegin = buffPtr;
    double value_arg = va_arg(argsPtr, double);
    if (tokenPtr->PRECISION < 0) {
        tokenPtr->PRECISION = 6;
    }
    int sign = 0;
    if (value_arg < 0) {
        sign = 1;
        value_arg *= -1;
    }
    char arr[256] = {};
    char *arrCursor = arr;

    long double intPart = 0;

    !tokenPtr->PRECISION ? intPart = roundl(value_arg) : modfl(value_arg, &intPart);

    tokenPtr->flag.space *= (tokenPtr->flag.plus || sign) ? 0 : 1;
    if (tokenPtr->flag.space) {
            *arrCursor = ' ';
            arrCursor++;
    }
    s21_itoa_p((long long)intPart, arrCursor);
    if (tokenPtr->PRECISION) {
        s21_strcat(arrCursor, ".");
    }
    if (tokenPtr->PRECISION) {
        char frac[256] = {};
        char *fracCursor = frac;
        long double ostatok_ld = value_arg - (long long)value_arg;
        int charsToWrite = tokenPtr->PRECISION + 1;
        while (charsToWrite--) {
            ostatok_ld *= 10;
            if (ostatok_ld < 1) {
                *fracCursor = '0';
                fracCursor++;
            }
        }
        long long ostatok_ll = (long long)ostatok_ld;
        ostatok_ll *= ostatok_ll < 0 ? -1 : 1;
        s21_itoa_p(ostatok_ll, fracCursor);
        int fraclen = s21_strlen(frac);
        while (fraclen++ < tokenPtr->PRECISION) {
            s21_strcat(arrCursor, "0");
        }
        s21_strcat(arrCursor, frac);
    }
    if (tokenPtr->PRECISION) {
        // сделаем округление
        int dotPlace = s21_strchr(arr, '.') - arr;
        int add = 0;
        // сначала пробегаемся по дробной части
        if (arr[dotPlace + tokenPtr->PRECISION + 1] >= '5') {
            add = 1;
        }
        arr[dotPlace + tokenPtr->PRECISION + 1] = '\0';
        // int i;
        for (int i = dotPlace + tokenPtr->PRECISION; i > dotPlace; i--) {
            arr[i] += add;
            add = 0;
            if (arr[i] > '9') {
                arr[i] = '0';
                add = 1;
            }
        }
        // теперь смотрим на целую часть
        if (add) {
            arr[dotPlace + 1] = '0';
            for (int i = dotPlace - 1; i >= 0; i--) {
                arr[i] += add;
                add = 0;
                if (arr[i] > '9') {
                    arr[i] = '0';
                    add = 1;
                }
            }
        }
        // обработка переполнения старшего разряда
        if (add) {
            for (int i = s21_strlen(arr); i > 0; i--) {
                arr[i] = arr[i - 1];
            }
            arr[0] = '1';
        }
    }
    int len_arr = s21_strlen(arr);
    if (sign || tokenPtr->flag.plus) {
        len_arr++;
    }
    int diffirent = tokenPtr->WIDTH - len_arr;
    if (tokenPtr->flag.minus) {                  // выравнивание по левому краю
        if (tokenPtr->flag.plus || sign) {
            *buffPtr = sign ? '-' : '+';
            buffPtr++;
        }
        s21_strcat(buffPtr, arr);
        buffPtr += s21_strlen(arr);
        for (int i = diffirent; i > 0; i--) {
            *buffPtr = ' ';
            buffPtr++;
        }
    } else {
        for (int i = diffirent; i > 0; i--) {
            *buffPtr = ' ';
            buffPtr++;
        }
        if (tokenPtr->flag.plus || sign) {
            *buffPtr = sign ? '-' : '+';
            buffPtr++;
        }
        s21_strcat(buffPtr, arr);
    }
    tokenPtr->fact_width = s21_strlen(bufferBegin);
return flag;
}
